using System;
using System.IO;
using System.Net.Http;
using System.Linq;
using System.Collections.Generic;
using System.Text.Json;
using System.Text;

namespace Intellio
{
    public class HttpEventStreamProcessor
    {
        public class EventPack
        {
            public Dictionary<string, string> Header { get; }
            public List<byte> Content { get; }

            public EventPack( Dictionary<string, string> header, List<byte> bytes )
            {
                Header = new Dictionary<string, string>( header );
                Content = new List<byte>( bytes );
            }

            public EventPack( EventImage src )
            {
                Header = src == null ? null : new Dictionary<string, string>( src.Header );
                Content = src == null ? null : new List<byte>( src.Content );
            }
        }

        public class EventImage
        {
            public Dictionary<string, string> Header { get; }
            public List<byte> Content { get; }

            public EventImage( Dictionary<string, string> header, List<byte> bytes )
            {
                Header = new Dictionary<string, string>( header );
                Content = new List<byte>( bytes );
            }

            public EventImage( EventImage src )
            {
                Header = src == null ? null : new Dictionary<string, string>( src.Header );
                Content = src == null ? null : new List<byte>( src.Content );
            }

            public EventImage( EventPack src )
            {
                Header = src == null ? null : new Dictionary<string, string>( src.Header );
                Content = src == null ? null : new List<byte>( src.Content );
            }

            public void saveAsJpegFile( string targetFilePath )
            { File.WriteAllBytes( targetFilePath, Content.ToArray() ); }
        }

        public class HttpEvent
        {
            public Dictionary<string, string> Header { get; }
            public EventImage AssociatedImage { get; }
            public JsonDocument JSONContent { get; }

            public HttpEvent( Dictionary<string, string> header, EventImage eventImage, JsonDocument jsonContent )
            {
                Header = new Dictionary<string, string>( header );
                AssociatedImage = new EventImage( eventImage );
                JSONContent = jsonContent;
            }

            public HttpEvent( HttpEvent src )
            {
                Header = src == null ? null : new Dictionary<string, string>( src.Header );
                AssociatedImage = src == null ? null : new EventImage( src.AssociatedImage );
                JSONContent = src == null ? null : src.JSONContent;
            }

            public void saveToFile( string targetFilePath )
            {
                using( var stream = new MemoryStream() )
                {
                    Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions { Indented = true });
                    JSONContent.WriteTo( writer );
                    writer.Flush();
                    File.WriteAllBytes( targetFilePath, stream.ToArray() );
                }
            }
        }

        public class HttpEventArgs
        {
            public HttpEvent Event { get; }

            public HttpEventArgs( HttpEvent _event )
            { Event = new HttpEvent( _event ); }
        }

        public delegate void HttpEventReceivedHandler( HttpEventStreamProcessor source, HttpEventArgs e);

        public event HttpEventReceivedHandler HttpEventReceived;

        public HttpEventStreamProcessor( string address, int port, string user, string password, int requestTimeoutSec = 60 )
        {
            RequestTimeoutSec = requestTimeoutSec;
            mUrl = @"http://" + address + ":" + port + @"/live/events?user=" + user + "&password=" + password;
        }

        public HttpEventStreamProcessor( string address, int port, string sessionId, int requestTimeoutSec = 60 )
        {
            RequestTimeoutSec = requestTimeoutSec;
            mUrl = @"http://" + address + ":" + port + @"/live/events?sid=" + sessionId;
        }

        public void startStream()
        {
            Console.Out.WriteLine( "Url: [" + mUrl + "]" );

            try
            {
                using( HttpClient httpClient = new HttpClient() )
                {
                    httpClient.Timeout = TimeSpan.FromSeconds( RequestTimeoutSec );
                                        
                    var request = new HttpRequestMessage( HttpMethod.Get, mUrl );
                    var response = httpClient.Send( request, HttpCompletionOption.ResponseHeadersRead );

                    // Determine boundary
                    if( !response.Content.Headers.Contains( "Content-Type" ) )
                    {
                        Console.Error.WriteLine( "[Error] HttpEventStreamProcessor: Cannot get multipart boundary: The response headers are missing Content-Type" );
                        return;
                    }
                    string boundary = determineBoundary( response.Content.Headers.GetValues( "Content-Type" ).First<string>() );
                    if( boundary.Length <= 0 )
                    {
                        Console.Error.WriteLine( "[Error] HttpEventStreamProcessor: Cannot get multipart boundary: missing from Content-Type" );
                        return;
                    }

                    string headerEnd = "\r\n\r\n";

                    // Read the stream
                    var stream = response.Content.ReadAsStream();
                    
                    int b = stream.ReadByte();

                    string headerAccumulator = "";
                    List<byte> binaryContentAccumulator = new List<byte>();
                    int expectedContentLength = -1;
                    
                    Dictionary<string, string> header = null;
                    EventPack eventPack = null;

                    Dictionary<long, EventImage> imageBuffer = new Dictionary<long, EventImage>();
                    
                    while( b > -1 )
                    { 
                        if( header == null ) // looking for header
                        {
                            headerAccumulator += System.Text.Encoding.UTF8.GetString( new byte[] { (byte) b } );
                            int indexOfBoundary = headerAccumulator.IndexOf( boundary );
                            if( indexOfBoundary > -1 )
                            {
                                int indexOfHeaderEnd = headerAccumulator.IndexOf( headerEnd );
                                if( indexOfHeaderEnd > -1 )
                                {
                                    int headerStartIndex = indexOfBoundary + boundary.Length;
                                    header = parseHeader( headerAccumulator.Substring( headerStartIndex, indexOfHeaderEnd - headerStartIndex ) );
                                    if( header == null )
                                    { Console.Out.WriteLine( "[Warning] HttpEventStreamProcessor: Failed to parse header" ); }
                                    expectedContentLength = int.Parse( header[ "Content-Length" ] );
                                    headerAccumulator = "";
                                }
                            }
                        }
                        else // reading content
                        {
                            binaryContentAccumulator.Add( (byte) b );
                            
                            if( binaryContentAccumulator.Count >= expectedContentLength )
                            {
                                eventPack = new EventPack( header, binaryContentAccumulator );
                                header = null;
                                expectedContentLength = -1;
                                binaryContentAccumulator.Clear();
                            }
                        }

                        if( eventPack != null ) // event pack complete
                        {
                            long timestamp = long.Parse( eventPack.Header[ "X-Timestamp" ] );
                            string contentType = eventPack.Header[ "Content-Type" ];
                            if( contentType == "image/jpeg" )
                            {
                                imageBuffer[timestamp] = new EventImage( eventPack );
                            }
                            else if( contentType == "application/json" )
                            {
                                // Event received, let's find the associated image
                                EventImage associatedImage = null;
                                if( imageBuffer.ContainsKey( timestamp ) )
                                { associatedImage = imageBuffer[ timestamp ]; }

                                // Let's trigger an event about the event
                                HttpEventReceived(
                                    this
                                ,   new HttpEventArgs(
                                        new HttpEvent(
                                            eventPack.Header
                                        ,   associatedImage
                                        ,   JsonDocument.Parse( System.Text.Encoding.UTF8.GetString( eventPack.Content.ToArray() ) )
                                        )
                                    )
                                );

                                { // Clean out old images
                                    var itemsToRemove = imageBuffer.Where( f => f.Key < timestamp ).ToArray();
                                    foreach( var item in itemsToRemove )
                                    { imageBuffer.Remove( item.Key ); }
                                }
                            }

                            eventPack = null;
                        }
                        b = stream.ReadByte();
                    }
                }
            }
            catch( Exception e )
            {
                Console.Error.WriteLine( "Failed to open stream:\n" + e.Message );
                return;
            }
        }

        private Dictionary<string, string> parseHeader( string headerContent )
        {
            if( headerContent.Length <= 0 )
            { return null; }

            Dictionary<string, string> header = new Dictionary<string, string>();
            var lines = headerContent.Split( '\n', 100, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries );
            foreach( string line in lines )
            {
                var parts = line.Split( ':', 2, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries );
                if( parts.Length != 2 )
                { continue; }

                header.Add( parts[0], parts[1] );
            }
            
            return header;
        }

        private string determineBoundary( string contentType )
        {
            int indexOfBoundary = contentType.IndexOf( "boundary=" );
            if( indexOfBoundary > -1 )
            {
                string boundary = contentType.Substring( indexOfBoundary + 9 );
                int indexOfSemicolon = boundary.IndexOf( ';' );
                if( indexOfSemicolon > -1 )
                { boundary = boundary.Substring( 0, boundary.Length - indexOfSemicolon + 1 ); }
                return "--" + boundary.Trim() + "\r\n";
            }
            return "";
        }

        public int     RequestTimeoutSec { get; set; }

        private string  mUrl;
    }
}

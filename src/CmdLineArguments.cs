using System;
using System.Collections.Generic;

namespace HttpEventStreamExample
{
    public class CmdLineArguments
    {
        public bool help = false;
        public string address = "";
        public int port = 80;
        public string user = "admin";
        public string password = "admin";
        public int requestTimeoutSec = 60;
        public bool dump = false;
        public string sessionId = "";

        private HashSet<string> mFlags = new HashSet<string>();

        public CmdLineArguments( string[] args )
        {
            mFlags.Add( "--port" );
            mFlags.Add( "-p" );
            mFlags.Add( "--user" );
            mFlags.Add( "-u" );
            mFlags.Add( "--password" );
            mFlags.Add( "-k" );
            mFlags.Add( "--timeout" );
            mFlags.Add( "-t" );
            mFlags.Add( "--dump" );
            mFlags.Add( "-d" );
            mFlags.Add( "--session-id" );
            mFlags.Add( "-s" );

            parseArgs( args );
        }

        private bool isFlag( string arg )
        {
            return mFlags.Contains( arg );
        }

        private void parseArgs( string[] args )
        {
            if( ( args.Length <= 0 ) || ( args.Length > 0 && ( args[0] == "--help" || args[0] == "-h" ) ) )
            {
                help = true;
                return;
            }

            for( int i = 0; i < args.Length-1; ++i )
            {
                if( isFlag( args[i] ) )
                {
                    if( (i+1) >= (args.Length-1) || isFlag( args[i+1] ) )
                    {
                        Console.Out.WriteLine( "[Warning] Argument missing for " + args[i] );
                        continue;
                    }

                    string flag = args[i];
                    ++i;

                    if( flag == "--port" || flag == "-p" )
                    {
                        try
                        {
                            port = int.Parse( args[i] );
                        }
                        catch( System.FormatException)
                        {
                            Console.Out.WriteLine( "[Warning] " + args[i] + " is an invalid argument for " + flag + " (must be a valid port number)" );
                            port = 80;
                        }

                        if( port <= 0 )
                        {
                            Console.Out.WriteLine( "[Warning] " + args[i] + " is an invalid argument for " + flag + " (must be a valid port number)" );
                            port = 80;
                        }
                    }
                    else if( flag == "--timeout" || flag == "-t" )
                    {
                        try
                        {
                            requestTimeoutSec = int.Parse( args[i] );
                        }
                        catch( System.FormatException)
                        {
                            Console.Out.WriteLine( "[Warning] " + args[i] + " is an invalid argument for " + flag + " (must be a positive integer number)" );
                            requestTimeoutSec = 60;
                        }

                        if( requestTimeoutSec <= 0 )
                        {
                            Console.Out.WriteLine( "[Warning] " + args[i] + " is an invalid argument for " + flag + " (must be a positive integer number)" );
                            requestTimeoutSec = 60;
                        }
                    }
                    else if( flag == "--user" || flag == "-u" )
                    { user = args[i]; }
                    else if( flag == "--password" || flag == "-k" )
                    { password = args[i]; }
                    else if( flag == "--dump" || flag == "-d" )
                    {
                        if( args[i] == "yes" )
                        { dump = true; }
                        else if( args[i] == "no" )
                        { dump = false; }
                        else
                        {
                            Console.Out.WriteLine( "[Warning] " + args[i] + " is an invalid argument for " + flag + " (must be yes, or no)" );
                            dump = false;
                        }
                    }
                    else if( flag == "--session-id" || flag == "-s" )
                    { sessionId = args[i]; }
                }
                else
                { Console.Out.WriteLine( "[Warning] Unknown flag: " + args[i] ); }
            }

            address = args[ args.Length - 1 ];
        }
    }
}

﻿using System;

namespace HttpEventStreamExample
{
    class Program
    {
        static void Main(string[] _args)
        {
            CmdLineArguments args = new CmdLineArguments( _args );
            if( args.help )
            {
                printHelp();
                return;
            }

            if( args.address.Length == 0 )
            {
                Console.Out.WriteLine( "[Error] Address must be provided (see usage below)!\n" );
                printHelp();
                return;
            }

            Console.WriteLine( "Configuration to be used:" );
            Console.WriteLine("    Address         : [" + args.address + "]");
            Console.WriteLine("    Port            : [" + args.port + "]");
            if( args.sessionId.Length > 0 )
            { Console.WriteLine("    Session-id      : [" + args.sessionId + "]"); }
            else
            {
                Console.WriteLine("    User            : [" + args.user + "]");
                Console.WriteLine("    Password        : [" + args.password + "]");
            }
            Console.WriteLine("    Request timeout : [" + args.requestTimeoutSec + "] seconds");
            Console.WriteLine("    Dump events     : " + (args.dump ? "[yes]" : "[no]"));
            Console.WriteLine("--------------------------------------------------------------------------------\n");

            new Program( args ).start();
        }

        private static void printHelp()
        {
            
            string helpMessage = @"
Usage:
    $ dotnet run -- [options] camera_address

    Options:
        --help       -h     Displays this help
        --port       -p     Port to be used (default: 80)
        --user       -u     Username for authentication on the camera (default: admin)
        --password   -k     Password for authentication on the camera (default: admin)
        --timeout    -t     Request timeout in seconds (default: 60)
        --dump       -d     If set to 'yes', the events and associated will be dumped out into files, into to current working direcotry
                            (valid arguments: yes no; default=no)
        --session-id -s     Session-id for authentication on the camera (default: "")
                            (Please note, if a session-id is provided, then that
                            will be used and the user and password arguments, if any
                            will be ignored.)

    Example:
      $ dontnet run -- --user Guard1 --password Potatoes --dump no 192.168.100.115
";
            Console.Out.WriteLine( helpMessage );
        }

        private Intellio.HttpEventStreamProcessor   mProcessor = null;
        bool                                        mDump = false;
        public Program( CmdLineArguments args )
        {
            mDump = args.dump;
            if( args.sessionId.Length > 0 )
            {
                mProcessor = new Intellio.HttpEventStreamProcessor(
                    args.address
                ,   args.port
                ,   args.sessionId
                ,   args.requestTimeoutSec
                );
            }
            else
            {
                mProcessor = new Intellio.HttpEventStreamProcessor(
                    args.address
                ,   args.port
                ,   args.user
                ,   args.password
                ,   args.requestTimeoutSec
                );
            }

            mProcessor.HttpEventReceived += onHttpEventReceived;
        }

        public void start()
        {
            mProcessor.startStream();
        }

        void onHttpEventReceived( Intellio.HttpEventStreamProcessor source, Intellio.HttpEventStreamProcessor.HttpEventArgs eventArgs )
        {
            string timeStampStr = eventArgs.Event.Header[ "X-Timestamp" ];

            // Print event info
            try
            {
                long timestamp = long.Parse( timeStampStr );
                string dateTime = new System.DateTime( 1970, 1, 1, 0, 0, 0, 0 ).AddMilliseconds( timestamp ).ToString( "yyyy'-'MM'-'dd HH':'mm':'ss'Z' (" + timestamp + ")" );
                string eventType = eventArgs.Event.JSONContent.RootElement.GetProperty( "DetectorEventType" ).ToString();
                string eventCode = eventArgs.Event.JSONContent.RootElement.GetProperty( "EventCode" ).ToString();
                string detectorClass = eventArgs.Event.JSONContent.RootElement.GetProperty( "DetectorClass" ).ToString();
                Console.Out.WriteLine( "\n\n" + dateTime + "\n    " + eventType + " (" + eventCode + ") from " + detectorClass );
            }
            catch( Exception )
            {
                Console.Out.WriteLine( "\n\nUnkown event" );
                return;
            }

            if( eventArgs.Event.AssociatedImage == null )
            {
                Console.Out.WriteLine( "    Associated image not found" );
                return;
            }

            try
            {
                string width = eventArgs.Event.AssociatedImage.Header[ "X-Frame-Width" ];
                string height = eventArgs.Event.AssociatedImage.Header[ "X-Frame-Height" ];
                string length = eventArgs.Event.AssociatedImage.Header[ "Content-Length" ];
                Console.Out.WriteLine( "    Associated image info: " + width + "x" + height + ", " + length + " bytes" );

            }
            catch( Exception )
            {
                Console.Out.WriteLine( "    Unkown image" );
                return;
            }

            if( mDump )
            {
                try
                {
                    eventArgs.Event.AssociatedImage.saveAsJpegFile( timeStampStr + ".jpg" );
                    eventArgs.Event.saveToFile( timeStampStr + ".json" );
                }
                catch( Exception e )
                { Console.Out.WriteLine( "Dump failed :S\n" + e.Message ); }
            }
        }
    }
}
